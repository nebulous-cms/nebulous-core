# nebulous-core #

Nebulous core to read in and use Nebulous data.

See the main docs on [https://nebulous-cms.org/core] on how the core engine works, what files it reads in,
and how it presents the data to the server.

## Links ##

|               | Website                                     | Twitter                                               |
|:-------------:|:-------------------------------------------:|:-----------------------------------------------------:|
| Project       | [Nebulous CMS](https://nebulous-cms.org/)   | [@NebulousCMS](https://twitter.com/NebulousCMS)       |
| Company       | [Nebulous Design](https://nebulous.design/) | [@NebulousDesign](https://twitter.com/NebulousDesign) |
| Author        | [Andrew Chilton](https://chilts.org/)       | [@andychilton](https://twitter.com/andychilton)       |

## License ##

ISC.

(Ends)
