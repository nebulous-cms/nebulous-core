// core
const path = require('path')

// npm
const test = require('tape')

// local
const nebulous = require('../')

// setup
const expected = {
  "url": "/",
  "config": {
    "title": "Deep Tree Structure"
  },
  "type": "dir",
  "meta": {
    "title": "The Index"
  },
  "content": {
    "one": {
      "url": "/one/",
      "type": "dir",
      "meta": {},
      "content": {
        "two-b": {
          "url": "/two-b/",
          "type": "dir",
          "meta": {},
          "content": {
            "three": {
              "url": "/three/",
              "type": "dir",
              "meta": {
                "title": "Three B"
              },
              "content": {}
            }
          }
        },
        "two-a": {
          "url": "/two-a/",
          "type": "dir",
          "meta": {},
          "content": {
            "three": {
              "url": "/three/",
              "type": "dir",
              "meta": {
                "title": "Three A"
              },
              "content": {}
            }
          }
        }
      }
    }
  },
  errors: [],
}

// tests
test('Test directory complex info', (t) => {
  t.plan(7)

  nebulous('test/90-deep', (err, data) => {
    t.ok(!err, 'No error when reading the content dir')

    // Check all dir parents, so we can remove them too.
    t.equal(data.content.one.parent, data, 'Parent is set correctly')
    delete data.content.one.parent
    t.equal(data.content.one.content['two-a'].parent, data.content.one, 'Parent of two-a is set correctly')
    delete data.content.one.content['two-a'].parent
    t.equal(data.content.one.content['two-b'].parent, data.content.one, 'Parent of two-b is set correctly')
    delete data.content.one.content['two-b'].parent
    t.equal(data.content.one.content['two-a'].content.three.parent, data.content.one.content['two-a'], 'Parent of two-a/three is set correctly')
    delete data.content.one.content['two-a'].content.three.parent
    t.equal(data.content.one.content['two-b'].content.three.parent, data.content.one.content['two-b'], 'Parent of two-b/three is set correctly')
    delete data.content.one.content['two-b'].content.three.parent

    // console.log('-------------------------------------------------------------------------------')
    // console.log(JSON.stringify(data, null, 2))
    // console.log('-------------------------------------------------------------------------------')

    t.deepEqual(data, expected, 'Correct data read')

    t.end()
  })
})
