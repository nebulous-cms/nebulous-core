
// core
const path = require('path')

// npm
const test = require('tape')

// local
const nebulous = require('../')

// setup
const expected = {
  url: '/',
  config: {},
  type: 'dir',
  meta: {},
  content: {},
  errors: [],
}

// tests
test('Test an empty dir', (t) => {
  t.plan(2)

  nebulous('test/10-empty', (err, data) => {
    t.ok(!err, 'No error when reading the empty dir')

    t.deepEqual(data, expected, 'Nothing is returned')

    t.end()
  })
})
