// core
const path = require('path')

// npm
const test = require('tape')

// local
const nebulous = require('../')

// setup
const expected = {
  url: '/',
  config: {},
  type: 'dir',
  meta: {},
  content: {
    index: {
      name: 'index',
      type: 'page',
      markup: 'markdown',
      meta: {
        title: 'Homepage',
      },
      content: 'Hello, World!\n',
      html: '<p>Hello, World!</p>\n',
    },
  },
  errors: [],
}

// tests
test('Test a simple install of just one file using an absolute path', (t) => {
  t.plan(14)

  const dir = path.join(__dirname, '40-simple')

  nebulous(dir, (err, data) => {
    t.ok(!err, 'No error when reading the simple data dir')

    t.equal(data.type, 'dir', 'Top-level type is dir')
    t.equal(data.url, '/', 'Top-level URL is correct')
    t.deepEqual(data.meta, {}, 'No meta expected')

    // Check parent first, so we can remove it for `deepEqual` checks, since it creates
    // a circular data structure.
    t.equal(data.content.index.parent, data, 'Parent is set correctly')
    delete data.content.index.parent

    // Check path.
    t.equal(data.content.index.path.length, 1, 'Path has one parent')
    t.equal(data.content.index.path[0], data, 'Parent is as expected')
    delete data.content.index.path

    t.equal(data.content.index.type, expected.content.index.type, 'Type is page')
    t.equal(data.content.index.name, expected.content.index.name, 'Name is index')
    t.equal(data.content.index.markup, expected.content.index.markup, 'Markup set as markdown')
    t.equal(data.content.index.content, expected.content.index.content, 'Content is correct')
    t.equal(data.content.index.html, expected.content.index.html, 'HTML is correct')
    t.deepEqual(data.content.index.meta, expected.content.index.meta, 'Meta is as expected')

    t.deepEqual(data, expected, 'Correct data read for the simple data dir')

    t.end()
  })
})

// tests
test('Test a simple install of just one file using a relative path', (t) => {
  t.plan(13)

  nebulous('test/40-simple', (err, data) => {
    t.ok(!err, 'No error when reading the simple data dir')

    t.equal(data.type, 'dir', 'Top-level type is dir')
    t.equal(data.url, '/', 'Top-level URL is correct')
    t.deepEqual(data.meta, {}, 'No meta expected')

    // Check parent first, so we can remove it for `deepEqual` checks, since it creates
    // a circular data structure.
    t.equal(data.content.index.parent, data, 'Parent is set correctly')
    delete data.content.index.parent

    // Check path.
    t.equal(data.content.index.path.length, 1, 'Path has one parent')
    t.equal(data.content.index.path[0], data, 'Parent is as expected')
    delete data.content.index.path

    t.equal(data.content.index.type, expected.content.index.type, 'Type is page')
    t.equal(data.content.index.markup, expected.content.index.markup, 'Markup set as markdown')
    t.equal(data.content.index.content, expected.content.index.content, 'Content is correct')
    t.equal(data.content.index.html, expected.content.index.html, 'HTML is correct')
    t.deepEqual(data.content.index.meta, expected.content.index.meta, 'Meta is as expected')

    t.deepEqual(data, expected, 'Correct data read for the simple data dir')

    t.end()
  })
})
