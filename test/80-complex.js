// core
const path = require('path')

// npm
const test = require('tape')

// local
const nebulous = require('../')

// setup
const expected = {
  url: '/',
  config: {},
  type: 'dir',
  meta: {
    menu: [ 'Item 1', 'Item 2' ],
  },
  content: {
    index: {
      type: 'page',
      name: 'index',
      markup: 'markdown',
      content: 'Twitter : [PizzaToppings](https://twitter.com/PizzaToppings).\n',
      html: '<p>Twitter : <a href="https://twitter.com/PizzaToppings">PizzaToppings</a>.</p>\n',
      meta: {
        title: 'Pizzas',
        subtitle: 'Bread, Toppings, and Techniques.',
      },
    },
    bread: {
      url: '/bread/',
      type: 'dir',
      meta: {
        menu: [ 'Neapolitan pizza' ],
      },
      content: {
        'neapolitan-pizza': {
          type: 'page',
          name: 'neapolitan-pizza',
          markup: 'markdown',
          content: 'https://www.youtube.com/watch?v=vV4gegZ7JNU\n',
          html: '<p>https://www.youtube.com/watch?v=vV4gegZ7JNU</p>\n',
          meta: {
            title: "Neapolitan Pizza",
          },
        },
      },
    },
    topping: {
      url: '/topping/',
      type: 'dir',
      meta: {
        title: 'Pizza Toppings',
      },
      content: {
        index: {
          type: 'page',
          name: 'index',
          markup: 'markdown',
          content: '## Pizza Toppings ##\n',
          html: '<h2>Pizza Toppings</h2>\n',
          meta: {
            title: 'Pizza Toppings',
          },
        },
      },
    },
  },
  errors: [],
}

// tests
test('Test directory complex info', (t) => {
  t.plan(17)

  nebulous('test/80-complex', (err, data) => {
    t.ok(!err, 'No error when reading the content dir')

    // Check all parents first, so we can remove it for `deepEqual` checks, since it creates
    // a circular data structure.
    t.equal(data.content.index.parent, data, 'Parent is set correctly')
    delete data.content.index.parent
    t.equal(data.content.bread.content['neapolitan-pizza'].parent, data.content.bread, 'Parent of pizza set correctly')
    delete data.content.bread.content['neapolitan-pizza'].parent
    t.equal(data.content.topping.content.index.parent, data.content.topping, 'Parent of topping.index set correctly')
    delete data.content.topping.content.index.parent

    // Check path.
    t.equal(data.content.index.path.length, 1, 'Path has one parent')
    t.equal(data.content.index.path[0], data, 'Parent is as expected')
    delete data.content.index.path
    t.equal(data.content.bread.content['neapolitan-pizza'].path.length, 2, 'Path for bread/pizza has one parent')
    t.equal(data.content.bread.content['neapolitan-pizza'].path[0], data, 'Parent 0 for bread/pizza is as expected')
    t.equal(data.content.bread.content['neapolitan-pizza'].path[1], data.content.bread, 'Parent 1 for bread/pizza is as expected')
    delete data.content.bread.content['neapolitan-pizza'].path
    t.equal(data.content.topping.content.index.path.length, 2, 'Path for topping index has one parent')
    t.equal(data.content.topping.content.index.path[0], data, 'Parent 0 for topping index is as expected')
    t.equal(data.content.topping.content.index.path[1], data.content.topping, 'Parent 1 for topping index is as expected')
    delete data.content.topping.content.index.path

    // Check all dir parents, so we can remove them too.
    t.equal(data.content.bread.parent, data, 'Parent of bread dir is correct')
    delete data.content.bread.parent
    t.equal(data.content.topping.parent, data, 'Parent of topping dir is correct')
    delete data.content.topping.parent

    t.deepEqual(data.content.bread.content, expected.content.bread.content, 'Bread content correct')
    t.deepEqual(data.content.topping.content, expected.content.topping.content, 'Topping content correct')
    t.deepEqual(data, expected, 'Correct data read')

    t.end()
  })
})
