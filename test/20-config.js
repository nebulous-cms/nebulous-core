
// core
const path = require('path')

// npm
const test = require('tape')

// local
const nebulous = require('../')

// setup
const expected = {
  url: '/',
  config: {
    apex: "example.org",
  },
  type: 'dir',
  meta: {},
  content: {},
  errors: [],
}

// tests
test('Test a config only content dir', (t) => {
  t.plan(2)

  nebulous('test/20-config', (err, data) => {
    t.ok(!err, 'No error when reading the empty dir')

    t.deepEqual(data, expected, 'Content is as expected')

    t.end()
  })
})
