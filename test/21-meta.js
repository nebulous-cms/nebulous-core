// core
const path = require('path')

// npm
const test = require('tape')

// local
const nebulous = require('../')

// setup
const expected = {
  url: '/',
  config: {},
  type: 'dir',
  meta: {
    menu: [ "Item 1", "Item 2" ],
  },
  content: {
    index: {
      type: 'page',
      name: 'index',
      markup: 'markdown',
      meta: {
        title: 'Title',
      },
      content: '',
      html: '',
    }
  },
  errors: [],
}

// tests
test('Test directory meta info', (t) => {
  t.plan(8)

  nebulous('test/21-meta', (err, data) => {
    t.ok(!err, 'No error when reading the content dir')

    // Check parent first, so we can remove it for `deepEqual` checks, since it creates
    // a circular data structure.
    t.equal(data.content.index.parent, data, 'Parent is set correctly')
    delete data.content.index.parent

    // Check path.
    t.equal(data.content.index.path.length, 1, 'Path has one parent')
    t.equal(data.content.index.path[0], data, 'Parent is as expected')
    delete data.content.index.path

    t.equal(data.type, 'dir', 'Top-level type is dir')
    t.equal(data.url, '/', 'Top-level URL is correct')
    t.deepEqual(data.meta, expected.meta, 'Directory meta info is correct')

    t.deepEqual(data, expected, 'Correct data read')

    t.end()
  })
})
