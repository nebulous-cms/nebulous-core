// core
const path = require('path')

// npm
const test = require('tape')

// local
const nebulous = require('../')

// setup
const expected = {
  url: '/',
  config: {},
  type: 'dir',
  meta: {},
  content: {
    index: {
      type: 'page',
      name: 'index',
      markup: 'markdown',
      meta: {
        title: 'Two Levels',
      },
      content: '',
      html: '',
    },
    recipe: {
      url: '/recipe/',
      type: 'dir',
      meta: {},
      content: {
        index: {
          type: 'page',
          name: 'index',
          markup: 'markdown',
          meta: {
            title: "Recipes",
          },
          content: '* Blue Cheese Surprise\n',
          html: '<ul>\n<li>Blue Cheese Surprise</li>\n</ul>\n',
        },
      },
    },
  },
  errors: [],
}

// tests
test('Test a two level content dir', (t) => {
  t.plan(28)

  nebulous('test/70-two-level', (err, data) => {
    t.ok(!err, 'No error when reading the two level content dir')

    t.equal(data.type, 'dir', 'Top-level type is dir')
    t.equal(data.url, '/', 'Top-level URL is correct')
    t.deepEqual(data.meta, {}, 'No meta expected')

    // Check parent first, so we can remove it for `deepEqual` checks, since it creates
    // a circular data structure.
    t.equal(data.content.index.parent, data, 'Parent is set correctly')
    delete data.content.index.parent
    t.equal(data.content.recipe.content.index.parent, data.content.recipe, 'Parent is set correctly')
    delete data.content.recipe.content.index.parent

    // Check path.
    t.equal(data.content.index.path.length, 1, 'Path has one parent')
    t.equal(data.content.index.path[0], data, 'Parent is as expected')
    delete data.content.index.path
    t.equal(data.content.recipe.content.index.path.length, 2, 'Path for recipe index has one parent')
    t.equal(data.content.recipe.content.index.path[0], data, 'Parent 0 for recipe index is as expected')
    t.equal(data.content.recipe.content.index.path[1], data.content.recipe, 'Parent 1 for recipe index is as expected')
    delete data.content.recipe.content.index.path

    // Check all dir parents, so we can remove them too.
    t.equal(data.content.recipe.parent, data, 'Parent of recipe dir is correct')
    delete data.content.recipe.parent

    t.equal(data.content.index.type, 'page', 'Correct top-level index type')
    t.equal(data.content.index.name, expected.content.index.name, 'Name is correct')
    t.equal(data.content.index.markup, 'markdown', 'Correct top-level markup')
    t.equal(data.content.index.content, expected.content.index.content, 'Correct top-level index content')
    t.equal(data.content.index.html, expected.content.index.html, 'Correct top-level index html')
    t.deepEqual(data.content.index.meta, expected.content.index.meta, 'Correct top-level index meta')

    t.equal(data.content.recipe.type, 'dir', 'Recipe type is dir')
    t.equal(data.content.recipe.url, '/recipe/', 'Recipe URL is correct')
    t.deepEqual(data.content.recipe.meta, {}, 'No meta expected')

    t.equal(data.content.recipe.content.index.type, 'page', 'Correct recipe index type')
    t.equal(data.content.recipe.content.index.name, expected.content.index.name, 'Name is correct')
    t.equal(data.content.recipe.content.index.markup, 'markdown', 'Correct recipe markup')
    t.equal(data.content.recipe.content.index.content, expected.content.recipe.content.index.content, 'Correct recipe index content')
    t.equal(data.content.recipe.content.index.html, expected.content.recipe.content.index.html, 'Correct recipe index html')
    t.deepEqual(data.content.recipe.content.index.meta, expected.content.recipe.content.index.meta, 'Correct recipe index meta')

    t.deepEqual(data, expected, 'Correct data read')

    t.end()
  })
})
