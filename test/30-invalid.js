
// core
const path = require('path')

// npm
const test = require('tape')

// local
const nebulous = require('../')

// setup
const expected = {
  url: '/',
  config: {},
  type: 'dir',
  meta: {},
  content: {},
  errors: [
    "Error parsing '_config.json': SyntaxError: Unexpected end of JSON input",
    "Error reading Markdown file 'index.md' - ignored: Error: ENOENT: no such file or directory, open 'test/30-invalid/index.md'",
    "Unknown type 'Bleurgh!' in 'invalid-type.json' - ignored.",
    "Error in 'redirect' page 'redirect.json' - no 'location' specified - ignored.",
  ],
}

// tests
test('Test various invalid types', (t) => {
  t.plan(2)

  nebulous('test/30-invalid', (err, data) => {
    t.ok(!err, 'No error when reading the invalid data dir')

    t.deepEqual(data, expected, 'Nothing is returned')

    t.end()
  })
})
