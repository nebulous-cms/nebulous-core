// core
const path = require('path')

// npm
const test = require('tape')

// local
const nebulous = require('../')

// setup
const expected = {
  url: '/',
  config: {},
  type: 'dir',
  meta: {},
  content: {
    index: {
      type: 'page',
      name: 'index',
      markup: 'markdown',
      meta: {
        title: 'My Homepage',
        subtitle: 'Where I put things on the internet.',
      },
      content: 'See : [chilts.org](https://chilts.org).\n',
      html: '<p>See : <a href="https://chilts.org">chilts.org</a>.</p>\n',
    },
    redirect: {
      type: 'redirect',
      name: 'redirect',
      location: 'https://zentype.com',
      meta: {
        type: 'redirect',
        location: 'https://zentype.com',
      },
    },
    html: {
      type: 'page',
      name: 'html',
      meta: {
        title: 'An HTML Page',
        markup: 'html',
      },
      html: '<p>Some HTML here.</p>\n',
    },
  },
  errors: [],
}

// tests
test('Test multiple content types', (t) => {
  t.plan(24)

  nebulous('test/50-multi', (err, data) => {
    t.ok(!err, 'No error when reading the multi data dir')

    t.equal(data.type, 'dir', 'Top-level type is dir')
    t.equal(data.url, '/', 'Top-level URL is correct')
    t.deepEqual(data.meta, {}, 'No meta expected')

    // Check all parents first, so we can remove it for `deepEqual` checks, since it creates
    // a circular data structure.
    t.equal(data.content.index.parent, data, 'Parent (page) is set correctly')
    delete data.content.index.parent
    t.equal(data.content.html.parent, data, 'Parent (html) is set correctly')
    delete data.content.html.parent

    // Check path.
    t.equal(data.content.index.path.length, 1, 'Path has one parent')
    t.equal(data.content.index.path[0], data, 'Parent is as expected')
    delete data.content.index.path
    t.equal(data.content.html.path.length, 1, 'Path has one parent')
    t.equal(data.content.html.path[0], data, 'Parent is as expected')
    delete data.content.html.path

    t.equal(data.content.index.type, expected.content.index.type, 'Type is page')
    t.equal(data.content.index.name, expected.content.index.name, 'Name is correct')
    t.equal(data.content.index.markup, expected.content.index.markup, 'Markup set as markdown')
    t.equal(data.content.index.content, expected.content.index.content, 'Content is correct')
    t.equal(data.content.index.html, expected.content.index.html, 'HTML is correct')
    t.deepEqual(data.content.index.meta, expected.content.index.meta, 'Meta is as expected')

    t.equal(data.content.redirect.type, expected.content.redirect.type, 'Type is redirect')
    t.equal(data.content.redirect.name, expected.content.redirect.name, 'Name is index')
    t.equal(data.content.redirect.location, expected.content.redirect.location, 'Location is correct')
    t.ok(!data.content.redirect.markup, 'No markup for redirect types')
    t.ok(!data.content.redirect.content, 'No content for redirect types')
    t.ok(!data.content.redirect.html, 'No html for redirect types')
    t.deepEqual(data.content.redirect.meta, expected.content.redirect.meta, 'Meta is as expected')

    t.deepEqual(data, expected, 'Correct data read for the multi data dir')

    t.end()
  })
})
