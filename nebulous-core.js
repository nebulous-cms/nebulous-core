// core
const fs = require('fs')
const path = require('path')

// npm
const fastGlob = require('fast-glob')
const async = require('async')
const MarkdownIt = require('markdown-it')

// setup
const md = new MarkdownIt()

function nebulous(dir, opts, callback) {
  if (!callback) {
    callback = opts
    opts = {}
  }

  const filenames = []

  // get all *.json files (the *.md files can be ignored for now)
  const stream = fastGlob.stream(
    [ '**' ],
    {
      cwd: dir,
      onlyFiles: false,
      markDirectories: true,
    }
  )
  stream.once('error', err => {
    callback(err)
  })

  console.log('Files:')
  stream.on('data', (entry) => {
    console.log(' * ' + entry)
    filenames.push(entry)
  })

  stream.once('end', () => {
    processFiles(dir, filenames.sort(), callback)
  })
}

function processFiles(dir, filenames, callback) {
  console.log('processFiles()')

  // start with an empty structure
  const data = {
    url: '/',
    config: {},
    type: 'dir',
    meta: {},
    content: {},
    errors: [],
  }

  async.eachSeries(
    filenames,
    (entry, done) => {
      console.log('entry=' + entry)
      if ( entry === '_config.json' ) {
        console.log(`Reading ${dir}/${entry} ...`)
        fs.readFile(path.join(dir, entry), 'utf8', (err, json) => {
          if (err) {
            // shouldn't really get here since this file was found okay
            console.warn("nebulous: error reading '_config.json':", err)
            data.errors.push("Error reading '_config.json': " + err)
            data.config = {}
            done()
            return
          }

          console.log('Got _config.json:', json)
          try {
            data.config = JSON.parse(json)
          }
          catch(err) {
            console.warn("nebulous: error parsing '_config.json':", err)
            data.errors.push("Error parsing '_config.json': " + err)
          }
          done()
        })
        return
      }

      if ( entry.match(/\/$/) ) {
        if ( entry === '/' ) {
          // nothing to do for the top-level
          done()
          return
        }

        console.log('Traversing down to the right dir ...')
        const parts = entry.replace(/\/$/, '').split('/')
        // create this 'dir' content type in the right place
        let current = data
        let previous = data
        let url = '/'
        for(let i = 0; i < parts.length; i++) {
          console.log('part:' + parts[i])
          current.content[parts[i]] = current.content[parts[i]] || {
            url     : url + parts[i] + '/',
            type    : 'dir',
            meta    : {},
            content : {},
          }
          current.content[parts[i]].parent = previous
          previous = current.content[parts[i]]
          current = current.content[parts[i]]
        }
        console.log('[data]', data)
        done()
        return
      }

      // ignore everything except JSON files
      if ( !entry.match(/\.json$/) ) {
        console.log('Ignoring : ' + entry)
        done()
        return
      }

      // get the basename (with no extension)
      entry = entry.replace(/\.json$/, '')

      // remove the filename, so we just have the directory path
      const parts = entry.split('/')
      const name = parts.pop()

      console.log('Reading : ' + entry)
      console.log('Parts   :', parts)
      console.log('Name    :', name)

      // walk the data until we get to the current dir
      let current = data
      for(let i = 0; i < parts.length; i++) {
        console.log(`Traversing down '${parts[i]}' ...`)
        current = current.content[parts[i]]
      }

      console.log('Current   :', current)

      // read the Meta
      fs.readFile(path.join(dir, entry + '.json'), 'utf8', (err, json) => {
        if (err) {
          done(err)
          return
        }

        const meta = JSON.parse(json)
        const type = meta.type || 'page'
        console.log('Type:', type)
        console.log('Meta:', meta)

        if ( name === '_meta' ) {
          current.meta = meta
          done()
          return
        }

        // depending on the type, load the relevant content file (if any)

        // redirect
        if ( type === 'redirect' ) {
          // check there is a location specified
          if ( !meta.location ) {
            console.warn(`nebulous: no location specified for 'redirect' type in '${entry}.json' - ignoring.`)
            data.errors.push(`Error in 'redirect' page '${entry}.json' - no 'location' specified - ignored.`)
            done()
            return
          }

          current.content[name] = {
            name: name,
            type: 'redirect',
            location: meta.location,
            meta,
          }
          done()
          return
        }

        if ( type === 'page' ) {
          // default to markdown
          const markup = meta.markup || 'markdown'

          // read any Markdown
          if ( markup === 'markdown' ) {
            fs.readFile(path.join(dir, entry + '.md'), 'utf8', (err, markdown) => {
              if (err) {
                console.warn(`nebulous: Error reading Markdown file '${entry}.md' :`, err)
                data.errors.push(`Error reading Markdown file '${entry}.md' - ignored: ` + err)
                done()
                return
              }

              // save this page
              const html = md.render(markdown)
              current.content[name] = {
                name: name,
                type: 'page',
                markup: 'markdown',
                content: markdown,
                html,
                meta,
                parent : current,
              }

              // generate the path on how to get here
              const path = []
              let here = current.content[name]
              while ( here.parent ) {
                path.push(here.parent)
                here = here.parent
              }
              current.content[name].path = path.reverse()
              done()
            })
            return
          }

          // read any HTML
          if ( markup === 'html' ) {
            const filename = entry + '.html'
            // read the HTML page
            fs.readFile(path.join(dir, filename), 'utf8', (err, html) => {
              if (err) {
                console.warn(`nebulous: Error reading HTML file '${filename}' :`, err)
                data.errors.push(`Error reading HTML file '${filename}' - ignored: ` + err)
                done()
                return
              }

              // save this page
              current.content[name] = {
                name: name,
                type: 'page',
                html,
                meta,
                parent : current,
              }

              // generate the path on how to get here
              const path = []
              let here = current.content[name]
              while ( here.parent ) {
                path.push(here.parent)
                here = here.parent
              }
              current.content[name].path = path.reverse()
              done()
            })
            return
          }

          // unknown markup
          console.warn(`nebulous: unknown markup '${markup}' at '${entry}.json' - ignoring.`)
          data.errors.push(`Unknown markup field '${markup}' in '${entry}.json' - ignored.`)
          done()
          return
        }

        // if we're here, then we don't know what the `type` is
        console.warn(`nebulous: unknown type '${type}' in '${entry}.json' - ignoring.`)
        data.errors.push(`Unknown type '${type}' in '${entry}.json' - ignored.`)
        done()
        return
      })

    },
    err => {
      callback(err, data)
    }
  )
}

// export just one function
module.exports = nebulous
